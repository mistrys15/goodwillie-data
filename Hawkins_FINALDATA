---
title: "Hawkins PercentCOver Gramanoids"
output:
  html_document:
    df_print: paged
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

```{r}
plot(cars)
```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
```{r}
STCTGRAM <- as.data.frame(read.csv("C:/Users/quinc/OneDrive/Desktop/goodwillie-data/stemcounts_gramanoids_CONVERTED.csv"))
```

```{r}
library(nlme)

model.a <- gls(STEM.COUNT ~ TREATMENT + BLOCK + YEAR + TREATMENT*BLOCK + TREATMENT*YEAR +BLOCK*YEAR, data=STCTGRAM)

ACF(model.a, form = ~ YEAR | QUADRAT.NUMBER)
```

```{r}
model <- lme(STEM.COUNT ~ TREATMENT + BLOCK + YEAR + TREATMENT*BLOCK + TREATMENT*YEAR + BLOCK*YEAR, random = ~1|QUADRAT.NUMBER, correlation = corAR1(form = ~ YEAR | QUADRAT.NUMBER, value=0.286868472), data = STCTGRAM, method = "REML")

library(car)

Anova(model)
```

```{r}
model.fixed <- gls(STEM.COUNT ~TREATMENT +BLOCK + YEAR + TREATMENT*BLOCK + TREATMENT*YEAR + BLOCK*YEAR, data= STCTGRAM, method = "REML")

anova(model, model.fixed)
```

```{r}
library(rcompanion)

model.null <- lme(STEM.COUNT ~ 1, random = ~1|QUADRAT.NUMBER, data = STCTGRAM)

nagelkerke(model, model.null)
```

```{r}
library(multcompView)
library(lsmeans)

marginal <- lsmeans(model, ~ TREATMENT:YEAR)

cld(marginal, alpha=0.05, Letters = letters, adjust = "tukey")
```

```{r}
Sum <- groupwiseMean(STEM.COUNT ~ TREATMENT + YEAR, data=STCTGRAM, conf = 0.05, digits =3, traditional = F, percentile = T)

Sum
```

```{r}
library(ggplot2)

pd <- position_dodge(.2)

ggplot(Sum, aes(x = YEAR, y = Mean, color = TREATMENT)) + 
  geom_errorbar(aes(ymin=Percentile.lower, ymax=Percentile.upper), width=.2, size=0.7, position=pd) +
  geom_point(shape=15, size=4, position = pd) +
  geom_smooth(method= "lm") +
  theme_bw() +
  theme(axis.title = element_text(face= "bold")) +
  ylab("Mean STEM COUNT per YEAR")
```

