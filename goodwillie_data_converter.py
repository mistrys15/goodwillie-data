#! /usr/bin/env python

import sys

input = sys.argv[1]

fp1=open(input, 'r+U')
fp2=open(input+"converted", 'w+')

lines = fp1.readlines()

for line in lines:
	if line.startswith("QUADRAT NUMBER"):
		fp2.write(line)
	else:
		segs = line.split(",")
		for seg in segs[4:]:
			fp2.write(segs[0]+","+segs[1]+","+segs[2]+","+seg+"\n")

fp1.close()
fp2.close()

